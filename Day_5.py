from random import shuffle, randrange

colour = ["blue", "green", "violet", "yellow", "pink", "red", "orange"]
grid = [[0 for j in range(9)] for i in range(9)]
total_score = 0


def print_grid(grid):
    for d in range(9):
        print(grid[d])


def generate_balls(grid, colour):
    count = 0
    while count < 3:
        row = randrange(9)
        col = randrange(9)
        shuffle(colour)
        ball = colour[1]
        if grid[row][col] == 0:
            grid[row].insert(col, ball)
            grid[row].pop(col + 1)
            count += 1


generate_balls(grid, colour)


def move_ball(row, col, row1, col1, grid):
    if grid[row1][col1] == 0:
        if grid[row][col] == "blue" or grid[row][col] == "violet" or grid[row][col] == "yellow" or grid[row][
            col] == "orange" or grid[row][col] == "pink" or grid[row][col] == "red" or grid[row][col] == "green":
            grid[row1][col1] = grid[row][col]
            grid[row].pop(col)
            grid[row].insert(col, 0)
        else:
            print("choose another row and coloum")
            get_inputs_from_user(grid)


    else:
        print("choose another row and coloum")
        get_inputs_from_user(grid)


def get_inputs_from_user(grid):
    print_grid(grid)
    row = int(input("enter the row which ball have to move :"))
    col = int(input("enter the col which ball have to move :"))
    row1 = int(input("enter the row where ball have to move :"))
    col1 = int(input("enter the col where ball have to move :"))
    return move_ball(row, col, row1, col1, grid)


def score(count, total_score):
    total_score += count * 2
    print(total_score)


def remove_balls_horizontal(grid, total_score):
    count = 1
    for a in range(9):
        for b in range(8):
            if grid[a][b] == grid[a][b + 1] and grid[a][b] != 0:
                count += 1
                if count > 4:
                    score(count, total_score)
                    for c in range(9):
                        if ((b + 1) - count) <= c <= (b + 1):
                            grid[a].pop(c)
                            grid[a].insert(c, 0)
                    get_inputs_from_user(grid)
            else:
                count = 1


def remove_balls_vertical(grid, total_score):
    count = 1
    for b in range(9):
        for a in range(8):
            if grid[a][b] == grid[a + 1][b] and grid[a][b] != 0:
                count += 1
                if count > 4:
                    score(count, total_score)
                    for c in range(9):
                        if (a + 1) - count <= c <= (a + 1) :
                            grid[c].pop(b)
                            grid[c].insert(b, 0)
                    get_inputs_from_user(grid)

            else:
                count = 1


for a in range(9):
    for b in range(9):
        if grid[a][b] == 0:
            get_inputs_from_user(grid)
            generate_balls(grid, colour)
            remove_balls_horizontal(grid, total_score)
            remove_balls_vertical(grid, total_score)

print_grid(grid)